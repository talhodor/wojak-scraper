import requests

WOJAK_URL = "https://wojakparadise.net/wojak/{0}/img"
IMAGE_PATH = "wojak_images/{0}.png"

def download_wojak(image_url):
    image_chunks = requests.get(image_url, stream=True)
    if image_chunks.status_code = 404:
        return False

    with open(image_url, 'wb') as image_file:
        for chunk in image_chunks:
            image_file.write(chunk)
    return True

for x in range(1,30,2):
    image_url = WOJAK_URL.format(x)
    print(f"Downloading {image_url} ...", end="")
    if download_wojak(image_url):
        print("Failed to download image")
    else:
        print("Download successful")